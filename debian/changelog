golang-github-xordataexchange-crypt (0.0.2+git20170626.21.b2862e3-3apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 12 Mar 2021 01:00:58 +0000

golang-github-xordataexchange-crypt (0.0.2+git20170626.21.b2862e3-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove Priority on golang-github-xordataexchange-crypt that duplicates
    source.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 04 Feb 2021 17:22:58 +0000

golang-github-xordataexchange-crypt (0.0.2+git20170626.21.b2862e3-2co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 15 Feb 2021 12:10:45 +0000

golang-github-xordataexchange-crypt (0.0.2+git20170626.21.b2862e3-2) unstable; urgency=medium

  * Team upload.

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Dr. Tobias Quathamer ]
  * Use debhelper v11
  * Update team name
  * Update to Standards-Version 4.2.1
    - Use Priority: optional

 -- Dr. Tobias Quathamer <toddy@debian.org>  Sat, 01 Dec 2018 22:13:21 +0100

golang-github-xordataexchange-crypt (0.0.2+git20170626.21.b2862e3-1) unstable; urgency=medium

  [ Tim Potter ]
  * Add me to uploaders

  [ Anthony Fok ]
  * New upstream version 0.0.2+git20170626.21.b2862e3
  * Change {Build-,}Depends: golang-etcd-dev to golang-etcd-server-dev:
    Upstream "Upgraded etcd library to current supported" on 2017-06-26
  * Use debhelper (>= 10)
  * Bump Standards-Version to 4.0.0:
    Use https form of the copyright-format URL in debian/copyright
  * Add "Testsuite: autopkgtest-pkg-go" to debian/control

 -- Anthony Fok <foka@debian.org>  Tue, 18 Jul 2017 00:19:20 -0600

golang-github-xordataexchange-crypt (0.0.2+git20150523.17.749e360-4) unstable; urgency=medium

  * Add debian/watch file
  * Refresh debian/control
    - Bump Standards-Version to 3.9.8 (no change)
    - Use secure https URL for Vcs-Git field too
    - Replace architecture-specific hack with golang-any in Build-Depends
    - Remove "golang-go | gccgo" in Depends for -dev package
    - Remove old transitional alternate dependency on golang-go.crypto-dev

 -- Anthony Fok <foka@debian.org>  Sun, 12 Jun 2016 21:13:18 -0600

golang-github-xordataexchange-crypt (0.0.2+git20150523.17.749e360-3) unstable; urgency=medium

  * Build using gccgo on architectures without golang-go
  * Allow package to be installed with gccgo where golang-go is unavailable
  * Update debian/copyright

 -- Anthony Fok <foka@debian.org>  Fri, 15 Jan 2016 01:00:23 -0700

golang-github-xordataexchange-crypt (0.0.2+git20150523.17.749e360-2) unstable; urgency=medium

  * Team upload.
  * Swap Build-Depends relation ordering to appease buildds (Closes: #802794)

 -- Tianon Gravi <tianon@debian.org>  Tue, 10 Nov 2015 06:04:03 -0800

golang-github-xordataexchange-crypt (0.0.2+git20150523.17.749e360-1) unstable; urgency=medium

  * Initial release (Closes: #799798)
  * Rename /usr/bin/crypt to /usr/bin/crypt-xordataexchange to avoid
    filename collision with the mcrypt package.
  * Add man page crypt-xordataexchange(1)

 -- Anthony Fok <foka@debian.org>  Tue, 22 Sep 2015 13:38:02 -0600
